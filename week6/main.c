#include <stdio.h>

void printArray(const int arr[], const int size);
void swap(int *p, int *k);
void stoogeSort(int arr[], const int low, const int high);

void swap(int *p, int *l){   
	int tempro;
	tempro = *p;
    *p = *l;
    *l =  tempro;
}

void stoogeSort(int arr[], const int low, const int high){	
	if(low >= high){
		return;
	}
	
	if(arr[low] > arr[high]){
		swap(&arr[low], &arr[high]);
	}
	
	if(high - low + 1 > 2){
		int k = (high - low +1) / 3;
		// First 2nd and 3rd index
		stoogeSort(arr, low, high - k);
		
		// Last 2nd and 3rd index
		stoogeSort(arr, low + k, high);
		
		// First 2nd and 3rd index again
		stoogeSort(arr, low, high - k);
	}
}

void printArray(const int arr[], const int size){
    printf("\n");
    for (int i = 0; i < size; i++){
        printf("%d\n", arr[i]);
    }
}

int main(void){
    printf("Enter the array size: ");
    int size;
    scanf("%d", &size);
    int arr[size];
    printf("Enter elements:\n");
    for (int i = 0; i < size; i++){
        int element;
        scanf("%d",&element);
        arr[i] = element;
    }
    
    stoogeSort(arr, 0, size - 1);
    
    printArray(arr, size);
	return 0;
}

