#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define countofSkaters 4
#define refereScoresNumber 6

typedef struct {
  char name[12], surname[12];
  double scores[6];
  double average;
}Skater;

double calculateScore(Skater *skater);
double findMaximum(double arr[]);
double findMinumum(double arr[]);
void loadSkaters(Skater skaters[], char file_name[]);

double calculateScore(Skater *skater) {
  double maximum, minumum, summary = 0;
  int i;
  for (i = 0; i < refereScoresNumber; i++) {
    summary += ( *skater).scores[i];
  }
  maximum = findMaximum(( *skater).scores);
  minumum = findMinumum(( *skater).scores);
  summary = summary - (maximum + minumum);
  return summary / 8;
}

double findMinumum(double arr[refereScoresNumber]) {
  double minumum = arr[0];
  int i;
  for (i = 0; i < refereScoresNumber; i++) {
    if (minumum > arr[i]) minumum = arr[i];
  }
  return minumum;
}

double findMaximum(double arr[]) {
  double maximum = arr[0];
  int i;
  for (i = 0; i < refereScoresNumber; i++) {
    if (maximum < arr[i]) maximum = arr[i];
  }
  return maximum;
}

void loadSkaters(Skater skaters[], char file_name[]) {
  double maximum = 0;
  int temp;
  int i = 0, j;
  FILE * output;
  output = fopen(file_name, "r");
  if (output != NULL) {
    while (!feof(output) && i != 4) {
      fscanf(output, "%s %s %lf %lf %lf %lf %lf %lf", skaters[i].name, skaters[i].surname, &skaters[i].scores[0], &skaters[i].scores[1], &skaters[i].scores[2], &skaters[i].scores[3], &skaters[i].scores[4], &skaters[i].scores[5]);
      skaters[i].average = calculateScore( & skaters[i]);
      printf("------------------\nSkater %d %s %s\nAverage point of Skater %d is: %.2f\n------------------\n\n", i + 1, skaters[i].name, skaters[i].surname, i + 1, skaters[i].average);
      i++;
    }
    printf("Loading Complete.\nFile closed.\n");
    fclose(output);
    for (j = 0; j < countofSkaters; j++) {
      if (maximum < skaters[j].average) {
        maximum = skaters[j].average;
        temp = j;
      }
    }
    printf("Winner of the Skating Championship is:\n%s %s with %.2f points in total.\n", skaters[temp].name, skaters[temp].surname, skaters[temp].average);
  }
}

int main() {
  char file[20];
  printf("Enter the file name to read: ");
  scanf("%s", file);
  printf("\n");
  FILE * fptr;
  fptr = fopen("skaters.txt", "w");
  fprintf(fptr, "%s %s %.0lf %.0lf %.0lf %.0lf %.0lf %.0lf\n", "Maria", "Solevaya", 4.0, 4.0, 5.0, 6.0, 4.0, 5.0);
  fprintf(fptr, "%s %s %.0lf %.0lf %.0lf %.0lf %.0lf %.0lf\n", "July", "Son", 6.0, 5.0, 5.0, 5.0, 6.0, 6.0);
  fprintf(fptr, "%s %s %.0lf %.0lf %.0lf %.0lf %.0lf %.0lf\n", "Alexandra", "Tatia", 5.0, 5.0, 6.0, 6.0, 4.0, 5.0);
  fprintf(fptr, "%s %s %.0lf %.0lf %.0lf %.0lf %.0lf %.0lf\n", "Joan", "Jung", 4.0, 4.0, 5.0, 3.0, 3.0, 4.0);
  fclose(fptr);

  Skater skaters[countofSkaters];
  loadSkaters(skaters, file);

  return 0;
}