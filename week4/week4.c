#include <stdio.h>
// Logoratima fonksiyonu icin mat
#include <math.h>
FILE *file;

// Global file degiskeni


// Arb. fonksiyonu
float arb_func(float x){
   return x * log10(x) - 1.2;
}

// False Position Methodu ile root bulma fonksiyonu
void rf(float *x, float x0, float x1, float fx0, float fx1, int *itr){
   *x = ((x0 * fx1) - (x1 * fx0)) / (fx1 - fx0);
   *itr=*itr+1;
   printf("Tekrar sayisi %d: %.5f\n", *itr, *x);
   fprintf(file, "Tekrar sayisi %d : %.4f\n", *itr, *x);
}

int main(){

   // Uzerine yazacagimiz dosya ve degiskenlerin declare edilmesi
   file = fopen("rf.txt", "w");
   float x, x0, x1, fx0, fx1, x_curr, x_next, error;
   int itr, maxitr;

   // Tolere edilecek deger alimi
   printf("[x0,x1] noktalari arasinda ki tolere edilebilecek aralarik degerlerini giriniz\n");
   printf("x0: ");
   scanf("%f", &x0);

   printf("x1: ");
   scanf("%f", &x1);

   printf("Hata payi: ");
   scanf("%f", &error);

   printf("Tekrar sayisi: ");
   scanf("%d", &maxitr);

   rf(&x_curr, x0, x1, arb_func(x0), arb_func(x1), &itr);


   /* 
      Bize verilen maks. tekrar sayisina uyacak sekilde
      en yakin koku bulma islemi
   */
   while (itr < maxitr){
      if (arb_func(x0) * arb_func(x_curr) < 0){
            x1 = x_curr;
      } else{
            x0 = x_curr;
      }

      rf(&x_next, x0, x1, arb_func(x0), arb_func(x1), &itr);
      if (fabs(x_next - x_curr) < error){
            printf("%d tekrardan sonra, bulunan kök: %.4f ", itr, x_curr);
            fprintf(file, "%d tekrardan sonra, bulunan kök: %.4f\n", itr, x_curr);
            return 0;
      } else{
            x_curr=x_next;
      }
   }
   
   // dosyayi da kapatalim dimi
   fclose(file);

   return 0;
}