#include <stdio.h>

int nrDigits(int num){
    if ((num / 10) > 0) {
        return (1 + nrDigits(num / 10));
    }
    else
        return(1);
}
int gcd(int num1, int num2) {
    if (num2 != 0)
        return gcd(num2, num1 % num2);
    else
        return num1;
}

int isPrime(int num, int divider){

    if (num == 0 || num == 1){
        return 0;
    }

    else if (num == 2){
        return 1;
    }
    else if (num < 0){
        return 0;
    }

    else{
        if(divider == 1){
            return 1;
         }
        else if(num % divider == 0){
            return 0;
        }
        else{
            return isPrime(num, divider - 1);
        }
    }
}

int main(){
    printf("%d",nrDigits(11111));
    printf("%d", isPrime(17,32));
    printf("%d", gcd(190,91));
}

